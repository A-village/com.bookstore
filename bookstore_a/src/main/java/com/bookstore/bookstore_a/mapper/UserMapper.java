package com.bookstore.bookstore_a.mapper;

import com.bookstore.api.bean.UserInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {
    //分页查询
    @Select("select * from user where user_state=1 limit #{start},#{size} ")
    List<UserInfo> findUserByPage(@Param("start") int start, @Param("size") int size);
    //共多少条数据
    @Select("select count(user_id) from user where user_state=1")
    int findAllCount();
    //登录验证
    @Select("select * from user where user_name=#{user_name} or user_phone=#{user_name} and user_pass=#{user_pass} and user_state=1")
    UserInfo login(UserInfo ui);
   //修改用户信息
    @Update("update user set user_name=#{user_name},user_email=#{user_email},user_prov=#{user_prov},user_city=#{user_city},user_phone=#{user_phone},postcode=#{postcode},address=#{address} where user_id=#{user_id} ")
    void update(UserInfo ui);
    //删除用户（将状态变成0）
    @Update("update user set user_state=0 where user_id=#{user_id}")
    void delete( int user_id);
    //添加用户
    @Insert("insert into user values(null,#{user_name},#{user_pass},#{user_phone},#{user_email},1,#{user_prov},#{user_city},#{postcode},#{address},1)")
    void insert(UserInfo ui);
    //查找用户---传过来的参数int id与#{id}中的绑定
    @Select("select * from user where user_id=#{id}")
    UserInfo findUserById(@Param("id") int id);
//@Select("select * from ")
}
