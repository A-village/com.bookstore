package com.bookstore.bookstore_a.mapper;

import com.bookstore.api.bean.OrderInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@Mapper
public interface OrderMapper {
    @Select("select * from `order`")
    List<OrderInfo>findAllOrder();
}
