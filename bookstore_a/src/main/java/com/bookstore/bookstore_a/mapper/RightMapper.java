package com.bookstore.bookstore_a.mapper;

import com.bookstore.api.bean.RightInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RightMapper {

    @Select("select * from `right`")
    List<RightInfo> findAllRight();

    @Select("select * from `right` where right_id = #{right_id}")
    RightInfo findRightById(int right_id);//通过id查询权限

}
