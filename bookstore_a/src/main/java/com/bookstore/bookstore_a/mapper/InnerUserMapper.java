package com.bookstore.bookstore_a.mapper;

import com.bookstore.api.bean.InnerUserInfo;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

@Mapper
public interface InnerUserMapper {

    @Select("select * from inneruser where inner_User_Phone = #{inner_User_Phone} and inner_User_Pass = #{inner_User_Pass}")
    InnerUserInfo login(InnerUserInfo innerUserInfo);//用户登录

    @Select("select * from inneruser where inner_user_state = 0 limit #{start},5")
    @Results(id = "innerMap",value = {
            @Result(property = "right",column = "right_id",
                    one = @One(select="com.bookstore.bookstore_a.mapper.RightMapper.findRightById",
                            fetchType = FetchType.EAGER))
    })
    List<InnerUserInfo> findInnerByPage(int start);//系统用户管理分页查询

    @Select("select count(inner_user_id) from inneruser")
    int findInnerByCount();//查询总行数

    @Update("update inneruser set inner_user_state = 1 where inner_User_Id = #{inner_User_Id}")
    void deleteUser(int inner_User_Id);//删除

    @Update("update inneruser set inner_User_Pass = #{inner_User_Pass},inner_User_Name = #{inner_User_Name}," +
            "right_id = #{right_id} where inner_User_Id = #{inner_User_Id}")
    void updateUser(InnerUserInfo innerUserInfo);//修改

    @Insert("insert into inneruser values(null,#{inner_User_Name},#{sex},#{inner_User_Phone},#{inner_User_Pass},#{right_id},0)")
    void saveInner(InnerUserInfo innerUserInfo);//添加

    @Select("select * from inneruser where right_id = #{right_id}")
    List<InnerUserInfo> findInnerById();
}
