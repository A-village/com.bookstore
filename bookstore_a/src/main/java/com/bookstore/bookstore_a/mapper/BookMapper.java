package com.bookstore.bookstore_a.mapper;

import com.bookstore.api.bean.AuthorInfo;
import com.bookstore.api.bean.BookInfo;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

/*book表的操作*/
@Mapper
public interface BookMapper {
    //全查询
    @Select("select * from book")
    List<BookInfo> findAllBooks();
    //book表与 author表联查
    /*@Select("SELECT * FROM book")
        @Results(id ="authors",value = {
                @Result(property = "author",column ="author_id",
                        one=@One(select =("com.bookstore.bookstore_a.mapper.AuthorMapper.findAuthorById"),
                                fetchType = FetchType.EAGER))
        })*/
    /*//图书表、作者表、出版社表、出版信息表 联查
    @Select("SELECT * FROM book")
    @Results(id ="authors_publisher",value = {
            @Result(property = "author",column ="author_id",
                    one=@One(select =("com.bookstore.bookstore_a.mapper.AuthorMapper.findAuthorById"),
                            fetchType = FetchType.EAGER)),
            @Result(property = "pubdetail",column = "book_id",
                    one = @One(select = ("com.bookstore.bookstore_a.mapper.PubdetailMapper.findPubliserById"),
                            fetchType = FetchType.EAGER))
    })
    List<BookInfo> findAllBooks();*/
    //图书表、作者表、出版社表、出版信息表 分页联查
    @Select("SELECT * FROM book limit #{start},#{size}")
    @Results(id ="authors_publisher",value = {
            @Result(property = "author",column ="author_id",
                    one=@One(select =("com.bookstore.bookstore_a.mapper.AuthorMapper.findAuthorById"),
                            fetchType = FetchType.EAGER)),
            @Result(property = "pubdetail",column = "book_id",
                    one = @One(select = ("com.bookstore.bookstore_a.mapper.PubdetailMapper.findPubliserById"),
                            fetchType = FetchType.EAGER))
    })
    List<BookInfo> findByPage(@Param("start") Integer start,@Param("size") Integer size);
    //查询所有数据条数
    @Select("SELECT COUNT(book_id) FROM book")
    Integer findBookCounts ();
    //添加图书
    /*@Insert("insert INTO book VALUES (#{},#{book_name},#{author_id},#{book_numbers},#{edition}," +
            "#{format},#{printed_sheet},#{isbn},#{price},#{type},#{column},#{brief_intro}',#{recommend})")*/
    @Insert("insert INTO book VALUES (null,#{book_name},#{author_id},#{book_numbers},#{edition},#{format},#{printed_sheet},#{isbn},#{price},null,null,#{brief_intro},#{recommend})")
    void saveBook(BookInfo bi);
    //查找新添加的图书的id
    @Select("SELECT book_id FROM `book` WHERE book_name =#{book_name} and isbn = #{isbn} ")
    Integer findNewBookId (BookInfo bi);
    //查找新添加的图书的id返回一个对象
    @Select("SELECT book_id FROM `book` WHERE book_name =#{book_name} and isbn = #{isbn} ")
    BookInfo findNew2BookId (BookInfo bi);
}
