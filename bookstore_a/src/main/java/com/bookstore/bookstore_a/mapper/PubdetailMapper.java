package com.bookstore.bookstore_a.mapper;

import com.bookstore.api.bean.PubdetailInfo;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

@Mapper
public interface PubdetailMapper {
   /* @Select("SELECT * FROM `pubdetail` WHERE information_id =#{information_id}")
    List<PubdetailInfo> findPubById(Integer information_id);*/
    //与出版社表联查得到出版社
    @Select("SELECT * FROM `pubdetail` WHERE information_id =#{information_id}")
    @Results(id = "publisherById",value = {
            @Result(property = "publisher",column ="publisher_id",
                    one=@One(select =("com.bookstore.bookstore_a.mapper.PublisherMapper.findPublisherById"),
                            fetchType = FetchType.EAGER))
    })
    List<PubdetailInfo> findPubliserById(Integer information_id);
    //添加book后加入与出版社的关联
    @Insert("insert into pubdetail values(null,#{book_id},#{publisher_id})")
    void savePubInfo (PubdetailInfo pubdetailInfo);
}
