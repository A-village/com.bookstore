package com.bookstore.bookstore_a.mapper;

import com.bookstore.api.bean.ColumnInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ColumnMapper {

    @Select("SELECT * FROM `column` ORDER BY column_index")
    List<ColumnInfo> findAllColumn();//查询

    @Delete("delete from `column` where column_id = #{column_id}")
    void deleteColumn(int column_id);//删除栏目

//    修改栏目名称
    @Update("update `column` set column_name=#{column_name} where column_id=#{column_id}")
    void update(@Param("column_id") int column_id, @Param("column_name") String column_name);
}
