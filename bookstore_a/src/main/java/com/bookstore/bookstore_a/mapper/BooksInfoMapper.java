package com.bookstore.bookstore_a.mapper;

import com.bookstore.api.bean.BookInfo;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface BooksInfoMapper {

    @Select("select * from book limit #{start},4")
    @Results(id = "bookMap",value = {
            @Result(property = "author",column = "author_id",
                    one = @One(select = "com.bookstore.bookstore_a.mapper.AuthorMapper.findAuthorById",
                            fetchType = FetchType.EAGER))
    })
    List<BookInfo> findSalableBooks(int start);//查询畅销书

    @Select("select count(book_id) from book")
    int findBookByCount();//查询总行数

    @Select("select * from book where book_id=#{book_id}")
    BookInfo findBookById(int book_id);

}
