package com.bookstore.bookstore_a.mapper;

import com.bookstore.api.bean.PublisherInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface PublisherMapper {
    @Select("SELECT * FROM publisher WHERE publisher_id = #{publisher_id}")
    List<PublisherInfo> findPublisherById(Integer publisher_id);
    //出版社全查询
    @Select("SELECT * FROM `publisher`")
    List<PublisherInfo> findAllPublishers();
}
