package com.bookstore.bookstore_a.mapper;

import com.bookstore.api.bean.AuthorInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/*作者表的操作*/
@Mapper
public interface AuthorMapper {
    @Select("SELECT * FROM author WHERE author_id =#{author_id}")
    AuthorInfo findAuthorById (Integer author_id);
    //作者全查
    @Select("SELECT * FROM `author`")
    List<AuthorInfo> findAllAuthors();
}
