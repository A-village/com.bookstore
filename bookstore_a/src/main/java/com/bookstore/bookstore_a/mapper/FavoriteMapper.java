package com.bookstore.bookstore_a.mapper;

import com.bookstore.api.bean.FavoriteInfo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface FavoriteMapper {

    @Insert("insert into favorite values(null,#{book_name},#{book_price},#{nowprice})")
    void saveFavorite(FavoriteInfo favoriteInfo);

    @Select("select * from favorite")
    List<FavoriteInfo> findFavorite();

    @Delete("delete from favorite where favorite_id = #{favorite_id}")
    void deleteFavorite(int favorite_id);

    @Select("select * from favorite where book_name = #{book_name}")
    FavoriteInfo findFavoriteById(String book_name);
}
