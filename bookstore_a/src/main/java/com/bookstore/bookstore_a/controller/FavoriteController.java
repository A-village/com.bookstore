package com.bookstore.bookstore_a.controller;

import com.bookstore.api.bean.FavoriteInfo;
import com.bookstore.bookstore_a.biz.FavoriteBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/favorite")
public class FavoriteController {

    @Autowired
    private FavoriteBiz favoriteBiz;

    @RequestMapping(value = "/save",produces = {"application/json;charset=utf-8"})
    public String saveFavorite(FavoriteInfo favoriteInfo){
        FavoriteInfo favorite = favoriteBiz.findFavoriteById(favoriteInfo.getBook_name());
        if(favorite == null){
            favoriteBiz.saveFavorite(favoriteInfo);
            return "redirect:/newfront/userFavorite.htm";
        }else {
            return "redirect:/newfront/prolist.html";
        }
    }

    @RequestMapping(value = "/find",produces = {"application/json;charset=utf-8"})
    @ResponseBody
    public List<FavoriteInfo> findFavorite(){
        return favoriteBiz.findFavorite();
    }

    @RequestMapping(value = "/del",produces = {"application/json;charset=utf-8"})
    public String deleteFavorite(int favorite_id) {
        favoriteBiz.deleteFavorite(favorite_id);
        return "redirect:/newfront/userFavorite.htm";
    }

}
