package com.bookstore.bookstore_a.controller;

import com.bookstore.api.bean.BookInfo;
import com.bookstore.bookstore_a.biz.BookBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/book")
public class BookController {
    @Autowired
    private BookBiz bookBiz;
    @RequestMapping(value = "/findAllBooks",produces = {"application/json;charset=utf-8"})
    public List<BookInfo> findAllBooks(){
        System.out.println(bookBiz.findAllBooks().toString());
        return bookBiz.findAllBooks();
    }
    //分页查询
    @RequestMapping(value = "/findByPage",produces = {"application/json;charset=utf-8"})
    public Map<String,Object> findBooksByPage
            (@RequestParam(name = "page",defaultValue = "1") Integer page,
             @RequestParam(name = "size",defaultValue = "5") Integer size){
        return bookBiz.findByPage(page,size);
    }
    //保存
    @RequestMapping(value = "/save")
    public String saveBook (BookInfo bi, HttpSession session){
        bookBiz.saveBook(bi);
        return "添加成功！";
    }
    //查找最新添加的
    @RequestMapping(value = "/findId",produces = {"application/json;charset=utf-8"})
    public int findNewBookId(BookInfo bookInfo){
        return bookBiz.findNewBookId(bookInfo);
    }
    //查找最新添加的,返回一个对象
    @RequestMapping(value = "/findId2",produces = {"application/json;charset=utf-8"})
    public BookInfo findNew2BookId(BookInfo bookInfo){
        return bookBiz.findNew2BookId(bookInfo);
    }
}
