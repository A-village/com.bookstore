package com.bookstore.bookstore_a.controller;

import com.bookstore.bookstore_a.biz.AuthorBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/author")
public class AuthorController {
    @Autowired
    private AuthorBiz authorBiz;
    @RequestMapping(value = "/findAll",produces = {"application/json;charset=utf-8"})
    public Map<String,Object> findAllAuthors(){
        return authorBiz.findAllAuthors();
    }
}
