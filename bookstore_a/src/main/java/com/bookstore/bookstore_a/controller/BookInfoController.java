package com.bookstore.bookstore_a.controller;

import com.bookstore.api.bean.BookInfo;
import com.bookstore.bookstore_a.biz.BookInfoBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RequestMapping("/bookinfo")
@Controller
public class BookInfoController {

    @Autowired
    private BookInfoBiz bookInfoBiz;

    @RequestMapping(value = "/sal",produces = {"application/json;charset=utf-8"})
    @ResponseBody
    public Map<String,Object> findSalableBooks(@RequestParam(defaultValue = "1")int page){
        return bookInfoBiz.findSalableBooks(page);
    }

    @RequestMapping(value = "/id",produces = {"application/json;charset=utf-8"})
    public String findBookById1(int book_id, HttpSession session){
        session.setAttribute("SELECT_BOOK",bookInfoBiz.findBookById(book_id));
        return "redirect:/newfront/userFavorite.htm";
    }

    @RequestMapping(value = "/id2",produces = {"application/json;charset=utf-8"})
    @ResponseBody
    public BookInfo findBookById2(HttpSession session){

        return (BookInfo) session.getAttribute("SELECT_BOOK");
    }
}
