package com.bookstore.bookstore_a.controller;

import com.bookstore.bookstore_a.biz.PublisherBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/pub")
public class PublisherController {
    @Autowired
    private PublisherBiz publisherBiz;
    @RequestMapping(value = "/findAll",produces = {"application/json;charset=utf-8"})
    public Map<String,Object> findAllPublisher(){
        Map<String,Object> map = new HashMap<>();
        map.put("ALL_PUB",publisherBiz.findAllPublishers());
        return map;
    }
}
