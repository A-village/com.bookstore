package com.bookstore.bookstore_a.controller;

import com.bookstore.api.bean.UserInfo;
import com.bookstore.bookstore_a.biz.UserBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Map;


@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserBiz ub;
    @RequestMapping(value = "/find",produces = {"application/json;charset=utf-8"})
    @ResponseBody
    public Map<String, Object> findAllUser(@RequestParam(defaultValue = "1") int page){
        System.out.println(ub.findUserByPage(page)+"aaaa");
        return ub.findUserByPage(page);
    }

    @RequestMapping("/login")
    public String login(UserInfo ui, HttpSession session){
        ui=ub.login(ui);
        if(ui!=null){
            session.setAttribute("USER_INFO",ui);
            return "redirect:/back/userInformationModify.htm";
        }else {
            return "redirect:/back/userlogin.htm";
        }

    }
    @RequestMapping("/logout")
    public String logout(UserInfo ui, HttpSession session){
       session.removeAttribute("USER_INFO");
        return "redirect:/back/userlogin.htm";
    }
    @GetMapping("/delete")
    public String delete(int user_id) {
        System.out.println(user_id+1111);
        ub.delete(user_id);
        return "redirect:/back/CustomerInformationList.htm";
    }
    @RequestMapping("/findone")
    public String findUserById(int id,HttpSession session) {
        System.out.println("AAAAAAAAAAAA"+id);
        System.out.println(ub.findUserById(id));
        session.setAttribute("ONE_USER",ub.findUserById(id));
        return "redirect:/back/CustomerInformationBrow.htm";
    }
    @RequestMapping(value = "/one",produces = {"application/json;charset=utf-8"})
    @ResponseBody
    public UserInfo findUserById2(HttpSession session) {
        return (UserInfo) session.getAttribute("ONE_USER");

        }
    @RequestMapping(value = "/update" )
    public String update(UserInfo ui){
        ub.update(ui);
        return "redirect:/back/CustomerInformationList.htm";
    }
    @RequestMapping(value = "/add" )
    public String add(UserInfo ui){
        ub.insert(ui);
        return "redirect:/back/CustomerInformationList.htm";
    }

}
