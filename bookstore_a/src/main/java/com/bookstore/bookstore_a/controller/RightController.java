package com.bookstore.bookstore_a.controller;

import com.bookstore.api.bean.RightInfo;
import com.bookstore.bookstore_a.biz.RightBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/right")
public class RightController {

    @Autowired
    private RightBiz rightBiz;

    @RequestMapping("/find")
    @ResponseBody
    public List<RightInfo> findAllRight(){
        return rightBiz.findAllRight();
    }
}
