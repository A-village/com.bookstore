package com.bookstore.bookstore_a.controller;

import com.bookstore.api.bean.ColumnInfo;
import com.bookstore.bookstore_a.biz.ColumnBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/column")
public class ColumnController {

    @Autowired
    private ColumnBiz columnBiz;

    @ResponseBody
    @RequestMapping(value = "/find",produces = {"application/json;charset=utf-8"})
    public List<ColumnInfo> findAllColumn(){
        return columnBiz.findAllColumn();
    }

    @RequestMapping(value = "/del",produces = {"application/json;charset=utf-8"})
    public String deleteColumn(int column_id){
        columnBiz.deleteColumn(column_id);
        return "redirect:/back/saloonTypeModify.htm";
    }

    @RequestMapping(value = "/upd",produces = {"application/json;charset=utf-8"})
    public String updColumn(int column_id,String column_name){
        columnBiz.update(column_id,column_name);
        return "redirect:/back/saloonTypeModify.htm";
    }
}
