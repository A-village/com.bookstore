package com.bookstore.bookstore_a.controller;

import com.bookstore.api.bean.OrderInfo;
import com.bookstore.bookstore_a.biz.OrderBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@Controller
@RequestMapping("/ord")
public class OrderController {
    @Autowired
    private OrderBiz ob;

    @ResponseBody
    @RequestMapping(value ="/find",produces = {"application/json;charset=utf-8"})
    public List<OrderInfo> findAllOrder(){
        System.out.println("aaaaa");
        return ob.findAllOrder();
    }
}
