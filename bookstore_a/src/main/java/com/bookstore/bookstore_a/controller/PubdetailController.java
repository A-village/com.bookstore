package com.bookstore.bookstore_a.controller;

import com.bookstore.api.bean.PubdetailInfo;
import com.bookstore.bookstore_a.biz.PubdetailBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pubDetail")
public class PubdetailController {
    @Autowired
    private PubdetailBiz pubdetailBiz;
    //插入信息
    @RequestMapping("/save")
    public String saveInfo(PubdetailInfo pubdetailInfo){
        pubdetailBiz.savePubInfo(pubdetailInfo);
        return ("bookid: "+pubdetailInfo.getBook_id()+"出版社id："+pubdetailInfo.getPublisher_id()+"关联成功");
    }
}
