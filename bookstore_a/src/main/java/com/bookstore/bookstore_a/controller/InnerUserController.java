package com.bookstore.bookstore_a.controller;

import com.bookstore.api.bean.InnerUserInfo;
import com.bookstore.bookstore_a.biz.InnerUserBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/inner")
public class InnerUserController {

    @Autowired
    private InnerUserBiz innerUserBiz;

    @RequestMapping("/login")
    public String login(InnerUserInfo innerUserInfo, HttpSession session){
        innerUserInfo = innerUserBiz.login(innerUserInfo);
        if (innerUserInfo == null){
            return "redirect:/back/login.html";
        }else {
            session.setAttribute("INNER_USER_INFO",innerUserInfo);
            return "redirect:/back/index.htm";
        }
    }

    @RequestMapping(value = "/page",produces = {"application/json;charset=utf-8"})
    @ResponseBody
    public Map<String,Object> findInnerByPage(@RequestParam(defaultValue = "1")int page){
        return innerUserBiz.findInnerByPage(page);
    }

    @RequestMapping(value = "/del",produces = {"application/json;charset=utf-8"})
    public String deleteInner(int inner_User_Id){
        innerUserBiz.deleteUser(inner_User_Id);
        return "redirect:/back/SysUserList.htm";
    }

    @RequestMapping(value = "/upd",produces = {"application/json;charset=utf-8"})
    public String updateInner(InnerUserInfo innerUserInfo){
        innerUserBiz.updateUser(innerUserInfo);
        return "redirect:/back/SysUserList.htm";
    }

    @RequestMapping(value = "/save",produces = {"application/json;charset=utf-8"})
    public String saveInner(InnerUserInfo innerUserInfo){
        innerUserBiz.saveInner(innerUserInfo);
        return "redirect:/back/SysUserList.htm";
    }

    /*@RequestMapping(value = "/id",produces = {"application/json;charset=utf-8"})
    public List<InnerUserInfo> findInnerById(){
        return innerUserBiz.findInnerById();
    }*/
}
