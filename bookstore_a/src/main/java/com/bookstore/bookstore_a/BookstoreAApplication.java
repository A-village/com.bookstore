package com.bookstore.bookstore_a;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookstoreAApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookstoreAApplication.class, args);
    }

}
