package com.bookstore.bookstore_a.biz.impl;

import com.bookstore.api.bean.PubdetailInfo;
import com.bookstore.bookstore_a.biz.PubdetailBiz;
import com.bookstore.bookstore_a.mapper.PubdetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service

public class PubdetailBizImpl implements PubdetailBiz {
    @Autowired
    private PubdetailMapper pubdetailMapper;
    @Override
    public void savePubInfo(PubdetailInfo pubdetailInfo) {
        pubdetailMapper.savePubInfo(pubdetailInfo);
    }
}
