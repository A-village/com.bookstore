package com.bookstore.bookstore_a.biz.impl;

import com.bookstore.api.bean.AuthorInfo;
import com.bookstore.bookstore_a.biz.AuthorBiz;
import com.bookstore.bookstore_a.mapper.AuthorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AuthorBizImpl implements AuthorBiz {
    @Autowired
    private AuthorMapper authorMapper;
    @Override
    public Map<String,Object> findAllAuthors() {
        Map<String,Object> authorMap = new HashMap<>();
        authorMap.put("ALL_AUTHORS",authorMapper.findAllAuthors());
        return authorMap;
    }
}
