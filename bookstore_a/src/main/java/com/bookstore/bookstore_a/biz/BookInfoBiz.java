package com.bookstore.bookstore_a.biz;

import com.bookstore.api.bean.BookInfo;

import java.util.List;
import java.util.Map;

public interface BookInfoBiz {

    Map<String,Object> findSalableBooks(int page);

    BookInfo findBookById(int book_id);
}
