package com.bookstore.bookstore_a.biz;

import com.bookstore.api.bean.BookInfo;

import java.util.List;
import java.util.Map;

public interface BookBiz {
    //全查询
    List<BookInfo> findAllBooks();
    //分页查询
    Map<String,Object> findByPage(Integer page, Integer size);
    //添加
    void saveBook(BookInfo bi);
    //查找新添加的图书id
    Integer findNewBookId (BookInfo bi);
    //查找新添加的图书id
    BookInfo findNew2BookId (BookInfo bi);
}
