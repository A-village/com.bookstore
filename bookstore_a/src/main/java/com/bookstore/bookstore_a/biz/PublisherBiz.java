package com.bookstore.bookstore_a.biz;

import com.bookstore.api.bean.PublisherInfo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PublisherBiz {
    //出版社全查询
    List<PublisherInfo> findAllPublishers();
}
