package com.bookstore.bookstore_a.biz;

import com.bookstore.api.bean.PubdetailInfo;
import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Service;

public interface PubdetailBiz {
    //添加book后加入与出版社的关联
    void savePubInfo (PubdetailInfo pubdetailInfo);
}
