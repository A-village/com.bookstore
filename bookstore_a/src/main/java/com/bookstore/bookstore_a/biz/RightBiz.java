package com.bookstore.bookstore_a.biz;

import com.bookstore.api.bean.RightInfo;

import java.util.List;

public interface RightBiz {

    List<RightInfo> findAllRight();

}
