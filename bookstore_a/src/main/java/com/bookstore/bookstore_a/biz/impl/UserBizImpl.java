package com.bookstore.bookstore_a.biz.impl;

import com.bookstore.api.bean.UserInfo;
import com.bookstore.bookstore_a.biz.UserBiz;
import com.bookstore.bookstore_a.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserBizImpl implements UserBiz {

    @Autowired
    private UserMapper um;
    public Map<String,Object> findUserByPage(int page){
        Map<String,Object> map=new HashMap<>();
        map.put("eachPageInfo",um.findUserByPage((page-1)*5,5));
        int allCount=um.findAllCount();
        map.put("allRows",allCount);
        map.put("allPages",allCount%5==0?(allCount/5):(allCount/5+1));
        System.out.println(map.keySet()+""+map.values());
        return map;
    }

    @Override
    public int findAllCount() {
        return um.findAllCount();
    }

    @Override
    public UserInfo login(UserInfo ui) {
        return um.login(ui);
    }

    @Override
    public void delete(int user_id) {
        um.delete(user_id);
    }

    @Override
    public UserInfo findUserById(int id) {
        return um.findUserById(id);
    }

    @Override
    public void update(UserInfo ui) {
        um.update(ui);
    }

    @Override
    public void insert(UserInfo ui) {
        um.insert(ui);
    }
}
