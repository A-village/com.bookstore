package com.bookstore.bookstore_a.biz.impl;

import com.bookstore.api.bean.FavoriteInfo;
import com.bookstore.bookstore_a.biz.FavoriteBiz;
import com.bookstore.bookstore_a.mapper.FavoriteMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoriteBizImpl implements FavoriteBiz {

    @Autowired
    private FavoriteMapper favoriteMapper;

    @Override
    public void saveFavorite(FavoriteInfo favoriteInfo) {
        favoriteMapper.saveFavorite(favoriteInfo);
    }

    @Override
    public List<FavoriteInfo> findFavorite() {
        return favoriteMapper.findFavorite();
    }

    @Override
    public void deleteFavorite(int favorite_id) {
        favoriteMapper.deleteFavorite(favorite_id);
    }

    @Override
    public FavoriteInfo findFavoriteById(String book_name) {
        return favoriteMapper.findFavoriteById(book_name);
    }
}
