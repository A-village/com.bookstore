package com.bookstore.bookstore_a.biz;

import com.bookstore.api.bean.ColumnInfo;

import java.util.List;

public interface ColumnBiz {

    List<ColumnInfo> findAllColumn();

    void deleteColumn(int column_id);

    void update(int column_id,String column_name);
}
