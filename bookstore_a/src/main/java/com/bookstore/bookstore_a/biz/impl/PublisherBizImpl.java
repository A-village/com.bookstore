package com.bookstore.bookstore_a.biz.impl;

import com.bookstore.api.bean.PublisherInfo;
import com.bookstore.bookstore_a.biz.PublisherBiz;
import com.bookstore.bookstore_a.mapper.PublisherMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PublisherBizImpl implements PublisherBiz {
    @Autowired
    private PublisherMapper publisherMapper;
    @Override
    public List<PublisherInfo> findAllPublishers() {
        return publisherMapper.findAllPublishers();
    }
}
