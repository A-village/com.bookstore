package com.bookstore.bookstore_a.biz;


import com.bookstore.api.bean.UserInfo;

import java.util.Map;

public interface UserBiz {
    Map<String,Object> findUserByPage(int page);
    int findAllCount();
    UserInfo login(UserInfo ui);
    void delete(int user_id);
    UserInfo findUserById(int id);
    void update(UserInfo ui);
    void insert(UserInfo ui);
}
