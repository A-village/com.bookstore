package com.bookstore.bookstore_a.biz.impl;

import com.bookstore.api.bean.RightInfo;
import com.bookstore.bookstore_a.biz.RightBiz;
import com.bookstore.bookstore_a.mapper.RightMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RightBizImpl implements RightBiz {

    @Autowired
    private RightMapper rightMapper;

    @Override
    public List<RightInfo> findAllRight() {
        return rightMapper.findAllRight();
    }
}
