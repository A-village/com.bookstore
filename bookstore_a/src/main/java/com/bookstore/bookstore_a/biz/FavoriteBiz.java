package com.bookstore.bookstore_a.biz;

import com.bookstore.api.bean.FavoriteInfo;

import java.util.List;

public interface FavoriteBiz {

    void saveFavorite(FavoriteInfo favoriteInfo);

    List<FavoriteInfo> findFavorite();

    void deleteFavorite(int favorite_id);

    FavoriteInfo findFavoriteById(String book_name);
}
