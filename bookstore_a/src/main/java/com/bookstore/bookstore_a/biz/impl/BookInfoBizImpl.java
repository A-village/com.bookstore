package com.bookstore.bookstore_a.biz.impl;

import com.bookstore.api.bean.BookInfo;
import com.bookstore.bookstore_a.biz.BookInfoBiz;
import com.bookstore.bookstore_a.mapper.BooksInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BookInfoBizImpl implements BookInfoBiz {

    @Autowired
    private BooksInfoMapper booksInfoMapper;

    @Override
    public Map<String,Object> findSalableBooks(int pge) {
        Map<String,Object> map = new HashMap<>();
        int all = booksInfoMapper.findBookByCount();
        map.put("num",all%4==0?(all/4):(all/4+1));
        map.put("rows",booksInfoMapper.findSalableBooks((pge-1)*4));
        return map;
    }

    @Override
    public BookInfo findBookById(int book_id) {
        return booksInfoMapper.findBookById(book_id);
    }
}
