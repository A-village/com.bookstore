package com.bookstore.bookstore_a.biz;

import com.bookstore.api.bean.AuthorInfo;

import java.util.List;
import java.util.Map;

public interface AuthorBiz {
    //作者全查
    Map<String,Object> findAllAuthors();
}
