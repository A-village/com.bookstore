package com.bookstore.bookstore_a.biz.impl;

import com.bookstore.api.bean.BookInfo;
import com.bookstore.bookstore_a.biz.BookBiz;
import com.bookstore.bookstore_a.mapper.BookMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BookBizImpl implements BookBiz {
    @Autowired
    private BookMapper bookMapper;
    @Override
    public List<BookInfo> findAllBooks() {
        return bookMapper.findAllBooks();
    }

    @Override
    public Map<String, Object> findByPage(Integer page, Integer size) {
        Map<String, Object> bookMap = new HashMap<>();
        bookMap.put("BOOKS_INFO_LIST",bookMapper.findByPage((page-1)*size,size));
        //map中放入总页数
        int allCounts = bookMapper.findBookCounts();
        bookMap.put("ALL_PAGE",allCounts%size==0?(allCounts/5):(allCounts/5+1));

        return bookMap;
    }

    //添加

    @Override
    public void saveBook(BookInfo bi) {
        bookMapper.saveBook(bi);
    }

    //查找新添加的图书id
    @Override
    public Integer findNewBookId(BookInfo bi) {
        return bookMapper.findNewBookId(bi);
    }

    //查找新添加的图书id返回一个对象。
    @Override
    public BookInfo findNew2BookId(BookInfo bi) {
        return bookMapper.findNew2BookId(bi);
    }
}
