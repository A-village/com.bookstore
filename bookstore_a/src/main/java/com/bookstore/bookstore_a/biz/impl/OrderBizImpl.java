package com.bookstore.bookstore_a.biz.impl;

import com.bookstore.api.bean.OrderInfo;
import com.bookstore.bookstore_a.biz.OrderBiz;
import com.bookstore.bookstore_a.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class OrderBizImpl implements OrderBiz {
    @Autowired
    private OrderMapper om;
    @Override
    public List<OrderInfo> findAllOrder() {
        return om.findAllOrder();
    }
}
