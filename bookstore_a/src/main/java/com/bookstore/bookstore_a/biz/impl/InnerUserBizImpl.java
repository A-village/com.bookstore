package com.bookstore.bookstore_a.biz.impl;

import com.bookstore.api.bean.InnerUserInfo;
import com.bookstore.bookstore_a.biz.InnerUserBiz;
import com.bookstore.bookstore_a.mapper.InnerUserMapper;
import com.bookstore.bookstore_a.mapper.RightMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InnerUserBizImpl implements InnerUserBiz {

    @Autowired
    private InnerUserMapper innerUserMapper;

    @Override
    public InnerUserInfo login(InnerUserInfo innerUserInfo) {
        return innerUserMapper.login(innerUserInfo);
    }

    @Override
    public Map<String, Object> findInnerByPage(int page) {
        Map<String,Object> map = new HashMap<>();
        int all = innerUserMapper.findInnerByCount();
        map.put("num",all%5==0?(all/5):(all/5+1));
        map.put("rows",innerUserMapper.findInnerByPage((page-1)*5));
        return map;
    }

    @Override
    public void deleteUser(int inner_User_Id) {
        innerUserMapper.deleteUser(inner_User_Id);
    }

    @Override
    public void updateUser(InnerUserInfo innerUserInfo) {
        innerUserMapper.updateUser(innerUserInfo);
    }

    @Override
    public void saveInner(InnerUserInfo innerUserInfo) {
        innerUserMapper.saveInner(innerUserInfo);
    }
}
