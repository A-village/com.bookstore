package com.bookstore.bookstore_a.biz;

import com.bookstore.api.bean.OrderInfo;

import java.util.List;

public interface OrderBiz {
    List<OrderInfo>findAllOrder();
}
