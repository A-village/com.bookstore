package com.bookstore.bookstore_a.biz;

import com.bookstore.api.bean.InnerUserInfo;

import java.util.List;
import java.util.Map;

public interface InnerUserBiz {

    InnerUserInfo login(InnerUserInfo innerUserInfo);

    Map<String,Object> findInnerByPage(int page);

    void deleteUser(int inner_User_Id);

    void updateUser(InnerUserInfo innerUserInfo);

    void saveInner(InnerUserInfo innerUserInfo);

}
