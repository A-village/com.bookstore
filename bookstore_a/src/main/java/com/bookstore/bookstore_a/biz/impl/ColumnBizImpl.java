package com.bookstore.bookstore_a.biz.impl;

import com.bookstore.api.bean.ColumnInfo;
import com.bookstore.bookstore_a.biz.ColumnBiz;
import com.bookstore.bookstore_a.mapper.ColumnMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ColumnBizImpl implements ColumnBiz {

    @Autowired
    private ColumnMapper columnMapper;

    @Override
    public List<ColumnInfo> findAllColumn() {
        return columnMapper.findAllColumn();
    }

    @Override
    public void deleteColumn(int column_id) {
        columnMapper.deleteColumn(column_id);
    }

    @Override
    public void update(int column_id,String column_name) {
        columnMapper.update(column_id,column_name);
    }
}
