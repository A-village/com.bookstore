package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FavoriteInfo {
    private Integer favorite_id;
    private Double book_price;
    private String book_name;
    private Double nowprice;
}
