package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeyWordInfo {
    private Integer keyword_id;
    private String keyword_context;
    private String reply_context;
    private Integer keyword_state;
}

