package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InnerUserInfo {
    private Integer inner_User_Id;
    private String inner_User_Name;
    private Integer sex;
    private String inner_User_Phone;
    private String inner_User_Pass;
    private Integer right_id;
    private Integer inner_user_state;
//    与权限表联查 通过right_id获取right_describe
    private RightInfo right;
}
