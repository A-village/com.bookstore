package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IntegralInfo {
    private Integer integral_id;
    private Integer user_id;
    private Integer available_integrals;
    private String total_integrals;
}
