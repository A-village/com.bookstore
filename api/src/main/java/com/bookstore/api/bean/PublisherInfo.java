package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PublisherInfo {
    private Integer publisher_id;
    private String pub_name;
    private String pub_province;
    private String pub_city;
    private String pub_detail;
}
