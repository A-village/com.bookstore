package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PubdetailInfo {
    private Integer information_id;
    private Integer book_id;
    private Integer publisher_id;

    //与出版社表联查
    private PublisherInfo publisher;
}
