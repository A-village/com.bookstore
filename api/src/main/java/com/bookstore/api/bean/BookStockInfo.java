package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookStockInfo {
    private Integer reserve_id;
    private Integer book_id;
    private Integer storage_quantity;
    private Integer inventory;
}
