package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EvaluateInfo {
    private Integer evaluate_id;
    private Integer book_id;
    private Integer user_id;
    private String evaluate_context;
    private Integer evaluate_state;
    private Integer evaluate_level;
}
