package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ColumnInfo {

    private Integer column_id;
    private String column_name;
    private Integer father_column_id;
    private Integer column_level;
    private Integer column_index;
}
