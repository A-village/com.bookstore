package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShoppingInfo {
    private Integer shopping_id;
    private Integer book_id;
    private Integer number;
}
