package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsersPaceInfo {
    private Integer user_space_id;
    private Integer user_id;
    private Integer book_id;
    private String share_content;
    private Integer order_id;
}
