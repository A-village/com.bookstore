package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShippingAddressInfo {
    private Integer ship_id;
    private Integer user_id;
    private String country;
    private String provincial;
    private String city;
    private String address;
    private String phone;
}
