package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {

    private Integer user_id;
    private String user_name;
    private String user_pass;
    private String user_phone;
    private String user_email;
    private Integer member_level;
    private Integer user_state;
    private String user_prov;
    private String user_city;
    private String postcode;
    private String address;
}
