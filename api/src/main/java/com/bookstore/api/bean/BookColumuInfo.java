package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookColumuInfo {
    private Integer column_id;
    private String column_sort;
    private String belong_column;
    private String rebate_grade;
}
