package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookAccesscaleInfo {
    private Integer access_id;
    private String date;
    private Integer peak;
    private Integer all_time_peak;
    private String all_time_peak_date;
}
