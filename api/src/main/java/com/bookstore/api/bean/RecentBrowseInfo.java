package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecentBrowseInfo {
    private Integer recent_browse_id;
    private Integer user_id;
    private Integer book_id;
    private String recent_browse_date;
}
