package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthorInfo {
    private Integer author_id;
    private String author_name;
    private String phone;
    private String address_country;
    private String address_province;
    private String address_city;
    private String address_location;
    private Integer author_state;
}
