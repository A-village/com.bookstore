package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderInfo {
    private Integer id;
    private  Integer order_number;
    private String orderdr;
    private  String order_booklist_detailed;
    private Integer user_number;
    private Integer address_number;
    private Double total;
    private String order_state;
    private String order_time;
    private String signing_time;
    private String payment_method;
    private String distribution_mode;
    private Integer using_integral;
    private Integer get_points;
}
