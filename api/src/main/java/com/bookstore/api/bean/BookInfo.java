package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookInfo {
    private Integer book_id;
    private String book_name;
    private Integer author_id;
    private String book_numbers;
    private String edition;
    private String format;
    private String printed_sheet;
    private String isbn;
    private String price;
    private String type;
    private String column;
    private String brief_intro;
    private String recommend;
    //通过author_id 与作者表一对一联查。获取作者信息
    private AuthorInfo author;
    //通过查找出版信息得到出版社id
    private PubdetailInfo pubdetail;
}
