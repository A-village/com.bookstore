package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReceiptInfo {
    private Integer receipt_id;
    private Integer user_id;
    private Integer order_id;
}
