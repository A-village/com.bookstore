package com.bookstore.api.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDatailInfo {
    private Integer order_id;
    private Integer order_number;
    private Integer book_number;
    private Integer order_amount;
}
